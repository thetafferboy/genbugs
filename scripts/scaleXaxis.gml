numberOfTicks = argument0;

var i = 0

while (i < (numberOfTicks + 1))
    {
    draw_line(global.gameMinX + 150 + xAxisLength * (i / numberOfTicks), global.gameMaxY - 150, global.gameMinX + 150 + xAxisLength * (i / numberOfTicks), global.gameMaxY - 140);  
    draw_text(global.gameMinX + 145 + xAxisLength * (i / numberOfTicks), global.gameMaxY - 130, string((global.graphLog * global.trackInterval) * (i / numberOfTicks)));
    i += 1;
    }
