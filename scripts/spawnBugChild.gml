// This controls bug reproduction

childGenes[0] = argument2;
childGenes[1] = argument3;
childGenes[2] = argument4;
childGenes[3] = argument5;
childGenes[4] = argument6;
childGenes[5] = argument7;
childGenes[6] = argument8;
childGenes[7] = argument9;
childGenes[8] = argument10;
nextGen = argument11 + 1;


// position of child
xmod = argument0 + irandom_range(-6,6);
ymod = argument1 + irandom_range(-6,6);

// make SUPER MUTANT?
superMutant = irandom(100);

if (!global.enableSpeedGene) {
    geneRange = 7;
    }
else {
    geneRange = 8;
    }

    selectGene = irandom(geneRange);
    
if superMutant <= global.superMutantChance {
    selectMutate = irandom_range(-100, 100);    
    }
else {
    selectMutate = irandom_range((global.maxMutate * -1), global.maxMutate);
    }
    
childGenes[selectGene] = (childGenes[selectGene] + selectMutate);

if (childGenes[selectGene] < 0) {childGenes[selectGene] = 0;}
if (childGenes[selectGene] > 100) {childGenes[selectGene] = 100;}

newBug = instance_create(xmod,ymod,obj_bug);
newBug.generation = nextGen;
for (i = 0; i < (geneRange + 1); i += 1) {
    newBug.genes[i] = childGenes[i];
    }

// make sure speed not set if disabled


