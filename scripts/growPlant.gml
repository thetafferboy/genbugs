flip = irandom(1);
    
if flip == 0 {xmod = argument0 - irandom(32);}
else {xmod = argument0 + irandom(16);}
    
flip = irandom(1);
if flip == 0 {ymod = argument1 - irandom(32);}
else {ymod = argument1 +irandom(16);}
    
// check to see if spawn area is clear
    
closestLeaf = instance_nearest(xmod, ymod, obj_leaf);
    
if point_distance(xmod,ymod,closestLeaf.x,closestLeaf.y) > 4 {
    instance_create(xmod,ymod,obj_leaf);
    }

